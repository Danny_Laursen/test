﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Webshop
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public WebShopEntities context;
        public MainWindow()
        {
            InitializeComponent();
            context = new WebShopEntities();
            PopulatelistBox();
        }

        public void PopulatelistBox()
        {
            var allCustomers = context.Customer.Select(x => x).ToList();

            lbCostumer.ItemsSource = allCustomers;
            lbCostumer.DataContext = allCustomers;

        }

        private void lbCustomer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedCustomer = lbCostumer.SelectedItem as Customer;
            txtCustomer.Text = selectedCustomer.firstName + " " + selectedCustomer.lastName;

            txtAddresse.Text = selectedCustomer.address;

            txtEmail.Text = selectedCustomer.eMail;

            var orderlist = selectedCustomer.Order.ToList();
            lbOrder.ItemsSource = orderlist;
            lbOrder.DataContext = orderlist;
        }

        private void LbOrder_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            lvInfo.Items.Clear();
            var selectedOrder = lbOrder.SelectedItem as Order;
            if (selectedOrder != null)
            {
                var orderLines = context.OrderLines.Where(x => x.orderId == selectedOrder.orderId).Select(x => x).ToList();
                foreach (var item in orderLines)
                {
                    lvInfo.Items.Add(new Product { title = item.Product.title, type = item.Product.type, country = item.Product.country, aging = item.Product.aging, kgPrice = item.Product.kgPrice });
                }
            }
        }
    }
}
